import { useState } from "react";

import TodoList from "../../components/Todo/TodoList";
import TodoInput from "../../components/Todo/TodoInput";

export default function Home() {
  const [todoList, setTodoList] = useState({
    data: [
      {
        id: 1,
        title: "Mengerjakan Exercise",
        completed: true,
        editing: true,
      },
      {
        id: 2,
        title: "Mengerjakan Assignment",
        completed: false,
        editing: true,
      },
    ],
  });

  const deleteTodo = (id) => {
    const todos = todoList.data.filter((item) => item.id !== id);
    setTodoList({ data: todos });
  };

  const addTodo = (newTodo) => {
    const todo = { id: Math.floor(Math.random() * 10000), ...newTodo };
    setTodoList({ data: [...todoList.data, todo] });
  };

  const checkTodo = (id) => {
    const todos = todoList.data.map((todo) => {
      if (todo.id === id) {
        todo.completed = !todo.completed;
      }
      return todo;
    });
    setTodoList({ data: todos });
  };

  return (
    <div>
      <h1 data-testid="head">Todo App</h1>
      <TodoInput addTodo={addTodo} />
      <TodoList
        data={todoList.data}
        deleteTodo={deleteTodo}
        checkTodo={checkTodo}
      />
    </div>
  );
}
