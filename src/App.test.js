import { render, screen } from "@testing-library/react";
import App from "./App";

it("Todo App Title", () => {
  render(<App />);
  expect(screen.getByTestId("head")).toHaveTextContent("Todo App");
});
